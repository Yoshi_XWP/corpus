<?php
/**
*@file
*Administration page callback for Corpus module
*/

/**
*Form builder. Configure corpus.
*
*@ingroup forms
*@see system_settings_form().
*/

function corpus_admin_settings() {
	$speakers = corpus_retrieve_speaker(); 
	$options_speaker = array();
	$i_s = 0;
	foreach ($speakers as $unit1) {
        $options_speaker[$i_s] = $unit1->speaker;
		$i_s = $i_s + 1;
	}
	$form['corpus_search_speaker'] = array(
        '#type' => 'select',
		'#empty_option' => '�������� ����������',
		'#options' => $options_speaker,
        );
	$form['#submit'][] = 'corpus_admin_settings_submit';
//	return system_settings_form($form);
}