<?php

function corpus_form($form, &$form_state) {
  $params = isset($_GET['q']) ? explode('/', $_GET['q']) : array();
  if ($params[0] == 'corpus' && $params[1] == 'edit') {
	$text_id = !empty($params[2]) && is_numeric($params[2]) ? $params[2] : '';

	$query = db_select('corpus_themes', 'n')
	  ->fields('n', array('theme_id', 'theme_name'))
	  ->orderBy ('theme_id', 'ASC')
	  ->execute();

	$themes_list = array();
	foreach ($query as $row) {
		$themes_list[$row->theme_id] = $row->theme_name;
	}

	$output = array('edit_form' =>
	  array(
		'speaker' => array(
		  '#type' => 'textfield',
		  '#title' => t('Speaker'),
		),
		'speaker_year' => array(
		  '#type' => 'textfield',
		  '#title' => t('Year of birth'),
		),
		'gender' => array(
		  '#type' => 'select',
		  '#title' => t('Gender'),
		  '#options' => array(1 => t('male'), 2 => t('female')),
		),
		'place' => array(
		  '#type' => 'textfield',
		  '#title' => t('Place'),
		),
		'year' => array(
		  '#type' => 'textfield',
		  '#title' => t('Year'),
		),
		'month' => array(
		  '#type' => 'textfield',
		  '#title' => t('Month'),
		),
		'day' => array(
		  '#type' => 'textfield',
		  '#title' => t('Day'),
		),
		'booknumber' => array(
		  '#type' => 'textfield',
		  '#title' => t('Book number'),
		),
		'comment' => array(
		  '#type' => 'textfield',
		  '#title' => t('Comment'),
		),
		'publish' => array(
		  '#type' => 'checkbox',
		  '#title' => t('Publish'),
		),
		'themes' => array(
		  '#type' => 'select',
		  '#empty_option' => t('Please select'),
		  '#options' => $themes_list,
		  '#attributes' => array('onchange' => 'corpus_handler.change_theme(this.options[this.selectedIndex].value);'),
		),
		'mark_button' => array(
		  '#type' => 'button',
		  '#value' => t('Mark'),
		  '#attributes' => array('onclick' => 'corpus_handler.mark_theme(); return false;', 'onsubmit' => 'return false;'),
		),
		'text' => array(
		  '#type' => 'text_format',
		  '#rows' => 25,
		  '#title' => t('Text'),
		  '#format' => 'corpus',
		),
		'text_id' => array(
		  '#type' => 'hidden',
		  '#value' => $text_id,
		),
		'submit_save' => array(
		  '#type' => 'submit',
		  '#value' => t('Save'),
		),
	  ),
	);
	if (!empty($text_id)) {
	  $text = db_select('corpus_text', 'n')
		->fields('n')
		->condition('n.text_id', $text_id)
		->execute()
		->fetchAssoc();
	  if (!empty($text)) {
		foreach($text as $k => $v) {
		  if (!empty($output['edit_form'][$k])) {
			$output['edit_form'][$k]['#default_value'] = $text[$k];
		  }
		}
	  }
	}

	return $output;
  }
  return array();
}

function return_corpus_form() {
  return drupal_get_form('corpus_form');
}

function corpus_form_submit($form, &$form_state) {
  $data = array(
	'text' => $form_state['values']['text']['value'],
	'speaker' => $form_state['values']['speaker'],
	'booknumber' => $form_state['values']['booknumber'],
	'year' => $form_state['values']['year'],
	'month' => $form_state['values']['month'],
	'day' => $form_state['values']['day'],
	'speaker_year' => $form_state['values']['speaker_year'],
	'gender' => $form_state['values']['gender'],
	'place' => $form_state['values']['place'],
	'comment' => $form_state['values']['comment'],
	'publish' => $form_state['values']['publish'],
  );
  if (!empty($form_state['values']['text_id'])) {
	$text_id = db_select('corpus_text', 'n')
	  ->fields('n', array('text_id'))
	  ->condition('n.text_id', $form_state['values']['text_id'])
	  ->execute()
	  ->fetchField();
  }
  if (!empty($text_id)) {
	db_update('corpus_text')
	  ->fields($data)
	  ->condition('text_id', $text_id)
	  ->execute();
  } else {
	$text_id = db_insert('corpus_text')
	  ->fields($data)->execute();
  }
  if ($text_id) {
	drupal_goto("/corpus/edit/".$text_id);
	drupal_set_message(t('Saved successfully'));
  } else {
	drupal_set_message(t('Error occured when saving text'));
  }
}

