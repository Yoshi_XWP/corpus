<?php


function corpus_retrieve_genres() {
 $query_genres = db_select('corpus_genres', 'n')
    ->fields('n', array('genre_id', 'genre_name'))
    ->orderBy('genre_id', 'ASC') //Most recent first.
    ->execute(); 
  return $query_genres;
}

 function show_genres() {
	 
	 
	$genres = corpus_retrieve_genres();
	$options_genre = array();
	$i_g = 0;
	foreach ($genres as $genre) {
        $options_genre[$i_g] = $genre->genre_name;
		$i_g = $i_g + 1;
	}
	 
	 
	 $output =  array(
		'form' => array(
		'#theme' => 'select',
		'#size'=> 9,
		'#options' => $options_genre,
        ),
	
  );
  return $output;
  
 }