<?php

error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
/**
*
* Help for Markup Editor.
*
* @param path 
*   Which path of the site we're using to display help
* @param arg 
*   Array that holds the current path as returned from arg() function
*/

function corpus_help($path, $arg) {
switch ($path) {
    case "admin/help#corpus":
      return '<p>' . "Здесь будет справка по подключению и настройке модуля \"Корпус\"" . '</p>';
      break;
  }
}


/**
*
*Search with parameters
*/

function corpus_retrieve_text() {
	$query_text = db_select('corpus_text', 'n')
	->fields('n', array('text'))
	->condition('text_id', 10, '=')
	->execute();
	return $query_text;
}

function corpus_retrieve_speaker() {
 $query_speaker = db_select('corpus_text', 'n')
    ->fields('n', array('speaker'))
	->distinct()
    ->orderBy('speaker', 'ASC') //Most recent first.
    ->execute(); 
  return $query_speaker;
}

function corpus_retrieve_year() {
 $query_r_year = db_select('corpus_text', 'n')
    ->fields('n', array('year'))
	->distinct()
    ->orderBy('year', 'ASC') //Most recent first.
    ->execute(); 
  return $query_r_year;
}

function corpus_retrieve_booknumber() {
 $query_booknumber = db_select('corpus_text', 'n')
    ->fields('n', array('booknumber'))
	->distinct()
    ->orderBy('booknumber', 'ASC') //Most recent first.
    ->execute(); 
  return $query_booknumber;
}


function corpus_form($form, &$form_state) {
	$form = array();


	//form simple search
	$form['simple_search'] = array(
		'#type' => 'textfield',
		'#size' => '30'
	);
	
	$form['submit_simple_search'] = array(
        '#type' => 'submit',
        '#value' => t('Search'),
    );

//	form speaker combo
	$speakers = corpus_retrieve_speaker(); 
	$options_speaker = array();
	$i_s = 0;
	foreach ($speakers as $unit1) {
        $options_speaker[$i_s] = $unit1->speaker;
		$i_s = $i_s + 1;
	}
	$form['corpus_search_speaker'] = array(
        '#type' => 'select',
		'#empty_option' => 'Выберите информанта',
		'#options' => $options_speaker,
        );
//  	form year combo
 	$years = corpus_retrieve_year();
	$options_r_years = array();
	$i_y = 0;
	foreach ($years as $unit2) {
        $options_r_years[$i_y] = $unit2->year;
		$i_y = $i_y + 1;
	}
	$form['corpus_search_year'] = array(
        '#type' => 'select',
		'#empty_option' => 'Выберите год',
		'#options' => $options_r_years,
        );		
		 
//	form booknumber combo
	$booknumbers = corpus_retrieve_booknumber();
	$options_booknumbers = array();
	$i_b = 0;
	foreach ($booknumbers as $unit3) {
        $options_booknumbers[$i_b] = $unit3->booknumber;
		$i_b = $i_b + 1;
	}

	$form['corpus_search_booknumber'] = array(
        '#type' => 'select',
		'#empty_option' => 'Выберите номер тетради',
		'#options' => $options_booknumbers,
        );

    $form['submit_params'] = array(
        '#type' => 'submit',
        '#value' => t('Search'),
    );
	
		$query = db_select('corpus_text', 'n')
		-> fields ('n', array('text_id', 'speaker', 'text'))
		-> orderBy ('speaker', 'ASC')
		-> execute();
		$header = array('Информант', 'Текст');
		$rows = array();
		
		$pattern = '/(#\d*)|(\d*#)/'; //remove markup
		
		foreach ($query as $node) {
			$rows[] = array($node->speaker, preg_replace($pattern, '', truncate_utf8(strip_tags($node->text), 500, TRUE)) . l('[подробней...]', 'corpus/text/'. $node->text_id)); 
		}
		
		$form['result_table'] = array(
		  '#theme' => 'table',
		  '#header' => $header,
		  '#rows' => $rows,
		  );
	
 	return $form; 
 
}

function return_corpus_form() {
	return drupal_get_form('corpus_form');
}



 function corpus_form_submit($form, &$form_state) {
	 $key_simple = $form_state['values']['simple_search'];
	 // $key_speaker = $form_state['values']['corpus_search_speaker'];
	 // $key_year = $form_state['values']['corpus_search_year'];
	 // $key_booknumber = $form_state['values']['corpus_search_booknumber'];
	 $key_simple = substr($key_simple, 0, 64); //limit the string to 64
	 $key_simple = preg_replace("/[^\w\x7F-\xFF\s]/", " ", $key_simple); //del all enormous symbols 
	 $good = trim(preg_replace("/\s(\S{1,2})\s/", " ", str_replace(" +", "  "," $key_simple "))); //forbid too short words for search
	 if ($good == '') {
		drupal_set_message("Слово для поиска слишком короткое");
	 }

   drupal_goto("/corpus/search/".$key_simple);
	// block_load('corpus', 'search');
}
