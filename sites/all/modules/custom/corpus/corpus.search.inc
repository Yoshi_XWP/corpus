<?php

/**
 * @param $key_search
 * @return string
 */
function show_search($key_search) {
  $query = db_select('corpus_text', 'n')
	->fields('n', array('text_id', 'speaker', 'text'))
	->where("REPLACE(n.text, '''', '') LIKE :search", array(':search' => '%' . db_like($key_search) . '%'))
  	->orderBy('speaker', 'ASC')
  ->execute();

  $header = array('Информант', 'Текст');
  $rows = array();

  $pattern = '/(#\d*)|(\d*#)/'; //remove markup

  $formatted_key = '';
  $count = mb_strlen($key_search);
  for($i = 0; $i < $count - 1; $i++)
  {
	$formatted_key .= mb_substr($key_search, $i, 1)."'?";
  }
  foreach ($query as $node) {
	$preformatted_text = preg_replace($pattern, '', strip_tags($node->text));
	preg_match('/'.$formatted_key.'/', $preformatted_text, $matches);
	foreach ($matches as $k => $v) {
	  $preformatted_text = preg_replace('/'.$v.'/', '<span style="background-color:yellow;">' . $v . '</span>', $preformatted_text);
	}
	$rows[] = array($node->speaker, truncate_utf8($preformatted_text, 500, TRUE) . l('[подробней...]', 'corpus/text/'. $node->text_id));
  }


  $output = theme('keysearch_result',
	array(
	  'key_search' => $key_search,
	  'table' => theme('table', array('header' =>$header, 'rows' =>$rows, )),


	)
  );
  return $output;
}

function theme_keysearch_result($variables) {
  $output = '<div>';
  $output .= '<p>Поиск по слову: <span style="background-color:yellow;">' . $variables['key_search'] . '</span></p>';
  $output .= '<div>'. $variables['table'].'</div>';
  $output .='</div>';

  return $output;
}