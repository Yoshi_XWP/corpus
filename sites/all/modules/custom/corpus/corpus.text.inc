<?php

/**
 * Page callback.
 */

 function show_full_text($textid = null) {
	$query = db_select('corpus_text', 'n')
		-> fields ('n')
		-> condition('n.text_id', $textid)
		-> execute()
		-> fetchAssoc();
		
	$pattern = '/(#\d*)|(\d*#)/'; //remove markup
		
	if ($query['speaker_year'] == 0) { 
		$speaker_year = 'неизвестен';
	} else {
		$speaker_year = $query['speaker_year'];
	}
	
	if ($query['gender'] == 1) {
		$gender = 'мужской';
	} else if ($query['gender'] == 2) {
		$gender = 'женский';
	} else {
		$gender = 'неизвестен';
	}
	
	if ($query['comment'] == '') {
		$comment = '--';
	} else {
		$comment = $query['comment'];
	}
	
	if ($query['day'] == 0) {
		$text_day = '--';
	} else {
		$text_day = $query['day'];
	}

	if ($query['month'] == 0) {
		$text_month = '--';
	} else {
		$text_month = $query['month'];
	}

	if ($query['year'] == 0) {
		$text_year = '----';
	} else {
		$text_year = $query['year'];
	}	
	
	$output = theme('single_text',
		array(
		    'id' => $textid,
			'speaker' => $query['speaker'],
			'date' => $text_day . '.' . $text_month . '.' . $text_year,
			'speaker_year' => $speaker_year,
			'gender' => $gender,
			'place' => $query['place'],
			'booknumber' => $query['booknumber'],
			'comment' => $comment,
			'text' => preg_replace($pattern, '', $query['text']),
		)
		
	);
  return $output;
 }
 
function theme_single_text($variables) {
    $output = '<a href="?q=corpus/edit/'.$variables['id'].'">Редактировать</a>';
	$output .= '<div>';
	$output .= '<p>Информант: <b>' . $variables['speaker'] . '</b></p>';
	$output .= '<p>Год рождения: <b>' . $variables['speaker_year']. '</b></p>';
	$output .= '<p>Пол: <b>' . $variables['gender']. '</b></p>';
	$output .= '<p>Место записи: <b>' . $variables['place']. '</b></p>';
	$output .= '<p>Дата записи: <b>' . $variables['date']. '</b></p>';
	$output .= '<p>Номер тетради: <b>' . $variables['booknumber']. '</b></p>';
	$output .= '<p>Комментарий: <b>' . $variables['comment']. '</b></p>';
	$output .=  '<div style="background-color:#E4E4E4;">'. $variables['text']. '</div>';
	$output .='</div>';
	return $output;
}