<?php


function corpus_retrieve_themes() {
 $query_themes = db_select('corpus_themes', 'n')
    ->fields('n', array('theme_id', 'theme_name'))
    ->orderBy('theme_id', 'ASC') //Most recent first.
    ->execute(); 
  return $query_themes;
}

 function show_themes() {
	 
	 
	$themes = corpus_retrieve_themes();
	$options_themes = array();
	$i_t = 0;
	foreach ($themes as $theme) {
        $options_themes[$i_t] = $theme->theme_name;
		$i_t = $i_t + 1;
	}
	 
	 
	 $output =  array(
		'form' => array(
		'#theme' => 'select',
		'#size'=> 62,
		'#options' => $options_themes,
        ),
	  );
  return $output;
  
 }